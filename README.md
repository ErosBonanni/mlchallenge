#  Mercado Libre Challenge

El challenge consistia en crear una app que utilice las APIs de Mercado Libre con dos secciones, search y detalle de producto, el resultado final es una pantalla de busqueda que puede filtrar por nombre de producto y categoría (Hay algunas categorias que tienen subcategorias, por cuestiones de tiempo no estoy manejando estos casos) y una pantalla de detalle del producto seleccionado.

## Detalles técnicos

La app se desarrolló en Xcode 11.1, Swift 5.1 con la arquitectura VIPER en mente, esta dividida en 3 modulos principales

- Products: Lista de productos con search bar y selector de categorias.
- Categories: Lista de categorias para seleccionar.
- ProductDetails: Detalle de producto.

En cuanto a networking, cree una extension de `URLSession` que se conforma al protocolo `NetworkingProvider` para poder testearlo mas facilmente y en un futuro si es necesario el cambio de `URLSession` a alguna otra libreria solo basta con conformar la clase nueva a `NetworkingProvider`. Tambien esta la clase `MercadoLibreAPI` que se inicializa con una base url y un networking provider, esta es la encargada de formatear las urls para pegarle a las apis de ML.

En cuanto a vistas use mayormente UITableViewControllers, en algunos casos use Interface Builder y en algunas celdas solo código.

## Ejecución

Para ejecutar la app solo basta con clonar el repositorio, seleccionar el scheme `MercadoLibreChallenge` y darle a run.
