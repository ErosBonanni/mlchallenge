//
//  ProductsViewTests.swift
//  MercadoLibreChallengeTests
//
//  Created by Eros Bonanni on 09/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import XCTest

@testable import MercadoLibreChallenge

class ProductsViewTests: XCTestCase {
    
    var presenter: ProductsPresenterMock!
    var view: ProductsViewController!
    
    override func setUp() {
        super.setUp()
        presenter = ProductsPresenterMock()
        view = ProductsViewController()
        view.presenter = presenter
    }
    
    func testViewDidLoad() {
        view.viewDidLoad()
        XCTAssert(presenter.viewDidLoadCalled)
    }
    
    func testDidSearch() {
        let searchBar = UISearchBar()
        searchBar.text = "Apple"
        view.searchBarSearchButtonClicked(searchBar)
        XCTAssert(presenter.didSearchForProductName == "Apple")
    }
    
    func testCategorySelect() {
        view.didTapSelectCategory()
        XCTAssert(presenter.didTapSelectCategoryButton)
    }
    
    func testRemoveFilters() {
        view.didTapRemoveFilter()
        XCTAssert(presenter.didTapRemoveFilters)
    }
    
    override func tearDown() {
        presenter = nil
        view = nil
    }
    
}
