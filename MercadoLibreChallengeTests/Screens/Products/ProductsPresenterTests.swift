//
//  ProductsPresenterTests.swift
//  MercadoLibreChallengeTests
//
//  Created by Eros Bonanni on 09/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import XCTest

@testable import MercadoLibreChallenge

class ProductsPresenterTests: XCTestCase {
    
    var view: ProductsViewMock!
    var router: ProductsRouterMock!
    var presenter: ProductsPresenter!
    var repository: ProductsRepositoryMock!
    
    override func setUp() {
        super.setUp()
        
        view = ProductsViewMock()
        router = ProductsRouterMock()
        repository = ProductsRepositoryMock()
        presenter = ProductsPresenter(interface: view,
                                      router: router,
                                      repository: repository,
                                      filters: [.name("iPhone")])
        
        view.presenter = presenter
        
    }
    
    func testEmptyProducts() {
        
        repository.productsToShow = []
        
        view.viewDidLoad()
        XCTAssert(view.hasStartedLoading)
        XCTAssert(repository.filters?.first?.id == "name")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            XCTAssert(self.view.isEmpty == true)
        }
    }
    
    func testLoaded() {
        
        repository.productsToShow = [ProductMock.product()]
        
        view.viewDidLoad()
        
        XCTAssert(view.hasStartedLoading)
        XCTAssert(repository.filters?.first?.id == "name")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            XCTAssert(self.view.products?.count == 1)
            XCTAssert(self.view.products?.first?.title == "iPhone 11 Pro")
        }
    }
    
    func testError() {
        repository.productsToShow = nil
        
        view.viewDidLoad()
        XCTAssert(view.hasStartedLoading)
        XCTAssert(repository.filters?.first?.id == "name")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            XCTAssert(self.view.errorDescription == "The operation couldn’t be completed. (MercadoLibreChallenge.NetworkingError error 2.)")
        }
    }
    
    func testCategoryButtonPressed() {
        presenter.didTapSelectCategory()
        XCTAssert(router.didShowCategorySelector == true)
    }
    
    func testDidSelectCategory() {
        presenter.didSelect(category: Category(id: "123", name: "Autos"))
        XCTAssert(view.categoryTitle == "Autos")
    }
    
    func testDidSelectProduct() {
        let selectedProduct = ProductMock.product()
        presenter.didSelect(product: selectedProduct)
        XCTAssert(router.productSelected?.title == selectedProduct.title)
    }
    
    func testDidSearch() {
        presenter.didSearch(for: "Apple")
        XCTAssert(repository.filters?.first?.id == "name")
    }
    
    func testRemoveFilters() {
        presenter.didSearch(for: "Apple")
        XCTAssert(repository.filters?.count == 1)
        presenter.removeFilters()
        XCTAssert(repository.filters?.count == 0)
    }
    
    override func tearDown() {
        view = nil
        router = nil
        repository = nil
        presenter = nil
        super.tearDown()
    }
    
}
