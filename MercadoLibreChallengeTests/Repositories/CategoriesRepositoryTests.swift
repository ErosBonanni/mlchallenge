//
//  CategoriesRepositoryTests.swift
//  MercadoLibreChallengeTests
//
//  Created by Eros Bonanni on 09/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import XCTest

@testable import MercadoLibreChallenge

class CategoriesRepositoryMock: XCTestCase {
    
    var mockedApi: MercadoLibreAPIMock!
    var repository: CategoriesRepository!
    
    override func setUp() {
        super.setUp()
        
        let mockedApi = MercadoLibreAPIMock()
        self.mockedApi = mockedApi
        
        self.repository = CategoriesRepository(api: mockedApi)
        
    }
    
    func testCategoriesUrl() {
        
        repository.categories { (_, _) in
            XCTAssert(self.mockedApi.path == "/sites/MLA/categories")
        }
        
    }
    
    override func tearDown() {
        mockedApi = nil
        repository = nil
        super.tearDown()
    }
    
}
