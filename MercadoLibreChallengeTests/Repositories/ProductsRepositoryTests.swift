//
//  ProductsRepositoryTests.swift
//  MercadoLibreChallengeTests
//
//  Created by Eros Bonanni on 09/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import XCTest

@testable import MercadoLibreChallenge

class ProductsRepositoryTests: XCTestCase {
    
    var mockedApi: MercadoLibreAPIMock!
    var repository: ProductsRepository!
    
    override func setUp() {
        super.setUp()
        
        let mockedApi = MercadoLibreAPIMock()
        self.mockedApi = mockedApi
        
        self.repository = ProductsRepository(api: mockedApi)
        
    }
    
    func testNameFilter() {
        repository.products(for: [.name("iPhone")]) { (response, error) in
            XCTAssert(self.mockedApi.path == "/sites/MLA/search?q=iPhone")
        }
    }
    
    func testCategoryFilter() {
        repository.products(for: [.category(id: "MLA1234")]) { (response, error) in
            XCTAssert(self.mockedApi.path == "/sites/MLA/search?category=MLA1234")
        }
    }
    
    func testsMultipleFilters() {
        repository.products(for: [.name("iPhone"), .category(id: "MLA1234")]) { (response, error) in
            XCTAssert(self.mockedApi.path == "/sites/MLA/search?q=iPhone&category=MLA1234")
        }
    }
    
    override func tearDown() {
        mockedApi = nil
        repository = nil
        super.tearDown()
    }
    
}
