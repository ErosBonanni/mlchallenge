//
//  ProductsViewMock.swift
//  MercadoLibreChallengeTests
//
//  Created by Eros Bonanni on 09/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

@testable import MercadoLibreChallenge

class ProductsViewMock: ProductsViewProtocol {
    
    var presenter: ProductsPresenterProtocol?
    
    func viewDidLoad() {
        presenter?.viewDidLoad()
    }
    
    var products: [Product]?
    var hasStartedLoading = false
    var errorDescription: String?
    var isEmpty = false
    
    func set(state: ProductsScreenState) {
        switch state {
            case .loaded(let products): self.products = products
            case .loading: self.hasStartedLoading = true
            case .empty: isEmpty = true
            case .error(let description): errorDescription = description
        }
    }
    
    var categoryTitle: String?
    
    func setCategory(title: String) {
        categoryTitle = title
    }
    
}
