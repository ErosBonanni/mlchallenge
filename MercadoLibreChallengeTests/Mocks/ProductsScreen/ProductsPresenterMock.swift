//
//  ProductsPresenterMock.swift
//  MercadoLibreChallengeTests
//
//  Created by Eros Bonanni on 09/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

@testable import MercadoLibreChallenge

class ProductsPresenterMock: ProductsPresenterProtocol {
    
    var viewDidLoadCalled = false
    func viewDidLoad() {
        viewDidLoadCalled = true
    }
    
    var didSearchForProductName: String?
    func didSearch(for productName: String) {
        didSearchForProductName = productName
    }
    
    var didSelectProduct: Product?
    func didSelect(product: Product) {
        didSelectProduct = product
    }
    
    var didTapSelectCategoryButton = false
    func didTapSelectCategory() {
        didTapSelectCategoryButton = true
    }
    
    var didTapRemoveFilters = false
    func removeFilters() {
        didTapRemoveFilters = true
    }
    
}
