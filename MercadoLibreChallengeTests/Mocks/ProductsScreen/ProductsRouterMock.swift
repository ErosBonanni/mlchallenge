//
//  ProductsRouterMock.swift
//  MercadoLibreChallengeTests
//
//  Created by Eros Bonanni on 09/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

@testable import MercadoLibreChallenge

class ProductsRouterMock: ProductsRouterProtocol {
    
    var productSelected: Product?
    
    func showProductDetails(_ product: Product) {
        productSelected = product
    }
    
    var didShowCategorySelector = false
    
    func showCategorySelector(with delegate: CategoriesSelectorDelegate) {
        didShowCategorySelector = true
    }
    
}
