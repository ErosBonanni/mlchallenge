//
//  Product.swift
//  MercadoLibreChallengeTests
//
//  Created by Eros Bonanni on 08/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

@testable import MercadoLibreChallenge

class ProductMock {
    
    static func product(id: String = "1",
                        siteId: String = "MLA",
                        title: String = "iPhone 11 Pro",
                        price: Double = 80000,
                        currencyId: String = "ARS",
                        thumbnail: String = "https://example.com/",
                        shipping: Shipping = Shipping(freeShipping: true, storePickUp: true),
                        condition: Condition? = .new,
                        installments: Installments? = Installments(quantity: 12, amount: 6666.66, rate: 0, currencyId: "ARS"),
                        seller: Seller = Seller(powerSellerStatus: .platinum)) -> Product {
        return Product(id: id,
                       siteId: siteId,
                       title: title,
                       price: price,
                       currencyId: currencyId,
                       thumbnail: thumbnail,
                       shipping: shipping,
                       condition: condition,
                       installments: installments,
                       seller: seller)
    }
    
}
