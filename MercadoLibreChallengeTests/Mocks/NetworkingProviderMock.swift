//
//  NetworkingProviderMock.swift
//  MercadoLibreChallengeTests
//
//  Created by Eros Bonanni on 08/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

@testable import MercadoLibreChallenge

class NetworkingProviderMock: NetworkingProvider {
    
    var urlCalled: String?
    var httpMethodCalled: HTTPMethod?
    
    func request<T>(_ url: String,
                    method: HTTPMethod,
                    headers: [String : String]?,
                    parameters: [String : String]?,
                    completion: @escaping (T?, Error?) -> Void) {
        
        guard let response = ProductMock.product() as? T else {
            return
        }
                
        urlCalled = url
        httpMethodCalled = method
        
        completion(response, nil)
        
    }
    
}
