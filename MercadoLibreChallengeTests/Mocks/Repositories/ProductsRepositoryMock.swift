//
//  ProductsRepositoryMock.swift
//  MercadoLibreChallengeTests
//
//  Created by Eros Bonanni on 09/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

@testable import MercadoLibreChallenge

class ProductsRepositoryMock: ProductsRepositoryProtocol {
    
    var productsToShow: [Product]? = []
    
    var filters: [ProductFilter]?
    
    func products(for filters: [ProductFilter], completion: @escaping (ProductsResponse?, Error?) -> Void) {
        self.filters = filters
        let paging = ProductsPaging(total: 1, offset: 0, limit: 1, primaryResults: 1)
        
        guard let productsToShow = productsToShow else {
            completion(nil, NetworkingError.timeout)
            return
        }
        
        let response = ProductsResponse(siteId: "MLA", results: productsToShow, paging: paging)
        completion(response, nil)
    }
    
}
