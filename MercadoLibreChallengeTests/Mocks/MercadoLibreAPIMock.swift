//
//  MercadoLibreAPIMock.swift
//  MercadoLibreChallengeTests
//
//  Created by Eros Bonanni on 09/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

@testable import MercadoLibreChallenge

class MercadoLibreAPIMock: API {
    
    var path: String?
    
    func get<T>(_ path: String,
                headers: [String : String]?,
                parameters: [String : String]?,
                completion: @escaping (T?, Error?) -> Void) where T : Decodable {
        
        self.path = path
        completion(nil, nil)
        
    }
    
}
