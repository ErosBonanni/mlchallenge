//
//  MercadoLibreAPITests.swift
//  MercadoLibreChallengeTests
//
//  Created by Eros Bonanni on 08/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import XCTest

@testable import MercadoLibreChallenge

class MercadoLibreAPITests: XCTestCase {
    
    var api: MercadoLibreAPI!
    var mockedNetworkingProvider: NetworkingProviderMock!
    
    override func setUp() {
        super.setUp()
        let mockedNetworking = NetworkingProviderMock()
        mockedNetworkingProvider = mockedNetworking
        api = MercadoLibreAPI(baseUrl: "https://api.mercadolibre.com",
                              networkingProvider: mockedNetworking)
    }
    
    func testGet() {
        api.get("/MLA/search?q=iPhone") { (product: Product?, error) in
            XCTAssert(self.mockedNetworkingProvider.urlCalled == "https://api.mercadolibre.com/MLA/search?q=iPhone")
            XCTAssert(self.mockedNetworkingProvider.httpMethodCalled == .get)
            XCTAssert(product?.title == "iPhone 11 Pro")
        }
    }
    
    override func tearDown() {
        api = nil
        mockedNetworkingProvider = nil
        super.tearDown()
    }
    
}
