//
//  ProductViewModelTests.swift
//  MercadoLibreChallengeTests
//
//  Created by Eros Bonanni on 08/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import XCTest

@testable import MercadoLibreChallenge

class ProductViewModelTests: XCTestCase {
    
    var productViewModel: ProductViewModel!
    
    override func setUp() {
        super.setUp()
        let product = ProductMock.product()
        productViewModel = ProductViewModel(product)
    }
    
    func testProductTitle() {
        XCTAssert(productViewModel.title == "iPhone 11 Pro")
    }
    
    func testProductPrice() {
        XCTAssert(productViewModel.price == "$80.000,00")
    }
    
    func testProductCondition() {
        XCTAssert(productViewModel.condition == "Nuevo")
    }
    
    func testUnknownCondition() {
        let product = ProductMock.product(condition: nil)
        let unknownConditionProductViewModel = ProductViewModel(product)
        XCTAssert(unknownConditionProductViewModel.condition == "Desconocido")
    }
    
    func testProductThumbnail() {
        XCTAssert(productViewModel.thumbnail == "https://example.com/")
    }
    
    func testFreeShipping() {
        XCTAssert(productViewModel.freeShipping == true)
    }
    
    func testProductInstallments() {
        XCTAssert(productViewModel.installments == "12 cuotas de $6.666,66")
    }
    
    func testProductWithoutInstallments() {
        let product = ProductMock.product(installments: nil)
        let withoutInstallmentsProductViewModel = ProductViewModel(product)
        XCTAssert(withoutInstallmentsProductViewModel.installments == "No hay cuotas disponibles")
    }
    
    func testSellerPowerStatus() {
        XCTAssert(productViewModel.sellerPowerStatus == "MercadoLíder Platinum")
    }
    
    override func tearDown() {
        productViewModel = nil
        super.tearDown()
    }
    
}
