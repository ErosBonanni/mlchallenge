//
//  ProductsRepository.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

protocol ProductsRepositoryProtocol {
    func products(for filters: ProductFilters, completion: @escaping (ProductsResponse?, Error?) -> Void)
}

class ProductsRepository: ProductsRepositoryProtocol {
    
    private let api: API
    
    init(api: API = MercadoLibreAPI.shared) {
        self.api = api
    }
    
    func products(for filters: ProductFilters, completion: @escaping(ProductsResponse?, Error?) -> Void) {
        let formattedPath = String(format: "/sites/MLA/search?%@", filters.uri())
        api.get(formattedPath, headers: nil, parameters: nil, completion: completion)
    }
    
}
