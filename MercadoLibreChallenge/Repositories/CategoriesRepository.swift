//
//  CategoriesRepository.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

protocol CategoriesRepositoryProtocol {
    func categories(completion: @escaping ([Category]?, Error?) -> Void)
}

class CategoriesRepository: CategoriesRepositoryProtocol {
    
    private let api: API
        
    init(api: API = MercadoLibreAPI.shared) {
        self.api = api
    }
    
    func categories(completion: @escaping ([Category]?, Error?) -> Void) {
        // I'm hardcoding the site to Mercado Libre Argentina.
        let path = "/sites/MLA/categories"
        api.get(path, headers: nil, parameters: nil, completion: completion)
    }
    
}
