//
//  HTTPMethod.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case get
    case post
    case put
    case path
    case delete
    
    var identifier: String {
        return rawValue
    }
    
}
