//
//  NetworkingError.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

enum NetworkingError: Error {
    
    case invalidUrl
    case unexpectedError
    case timeout
    
}
