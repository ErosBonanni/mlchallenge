//
//  API.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

protocol API {
    
    func get<T: Decodable>(_ path: String,
              headers: [String: String]?,
              parameters: [String: String]?,
              completion: @escaping RequestCompletion<T>)
    
}
