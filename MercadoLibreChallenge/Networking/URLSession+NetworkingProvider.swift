//
//  URLSession+NetworkingProvider.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

/// Extends URLSession to conform Networking Provider protocol.
extension URLSession: NetworkingProvider {
    
    func request<T: Decodable>(_ url: String,
                               method: HTTPMethod,
                               headers: [String : String]?,
                               parameters: [String : String]?,
                               completion: @escaping RequestCompletion<T>) {
        
        guard let url = URL(string: url) else {
            completion(nil, NetworkingError.invalidUrl)
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = method.identifier
        
        if let headers = headers {
            request.allHTTPHeaderFields = headers
        }
        
        if let parameters = parameters {
            
            do {
                let encoder = JSONEncoder()
                let body = try encoder.encode(parameters)
                request.httpBody = body
            } catch (let error) {
                completion(nil, error)
                return
            }
            
        }
        
        dataTask(with: request) { (data, response, error) in
            if let error = error, data == nil {
                completion(nil, error)
            }
            
            guard let data = data else {
                completion(nil, NetworkingError.unexpectedError)
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let object = try decoder.decode(T.self, from: data)
                completion(object, nil)
            } catch(let error) {
                completion(nil, error)
                return
            }
        }.resume()
        
    }
    
}

