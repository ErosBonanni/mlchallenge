//
//  MercadoLibreAPI.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

class MercadoLibreAPI: API {
    
    static let shared = MercadoLibreAPI(baseUrl: "https://api.mercadolibre.com",
                                        networkingProvider: URLSession.shared)
    
    private let baseUrl: String
    private let networkingProvider: NetworkingProvider
    
    init(baseUrl: String, networkingProvider: NetworkingProvider) {
        self.networkingProvider = networkingProvider
        self.baseUrl = baseUrl
    }
    
    func get<T: Decodable>(_ path: String,
                           headers: [String : String]? = nil,
                           parameters: [String : String]? = nil,
                           completion: @escaping RequestCompletion<T>) {
        
        networkingProvider.request(baseUrl + path,
                                   method: .get,
                                   headers: headers,
                                   parameters: parameters,
                                   completion: completion)
    }

}
