//
//  NetworkingProvider.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

typealias RequestCompletion<T: Decodable> = (T?, Error?) -> Void

protocol NetworkingProvider {
    
    func request<T: Decodable>(_ url: String,
                               method: HTTPMethod,
                               headers: [String: String]?,
                               parameters: [String: String]?,
                               completion: @escaping RequestCompletion<T>)
    
}
