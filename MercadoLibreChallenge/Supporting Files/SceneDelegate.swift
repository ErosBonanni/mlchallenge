//
//  SceneDelegate.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        // First we make sure that there is a scene, then we create
        // the window with the respective root controller and assign
        // it to the scene.
        
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            let products = ProductsRouter.createModule(for: [QueryFilter("iPhone")])
            window.rootViewController = MLNavigationController(rootViewController: products)
            self.window = window
            window.makeKeyAndVisible()
        }
        
    }
    
}
