//
//  UIImage+URL+Cache.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import UIKit

extension UIImageView {
    func loadImage(from url: String, cache: URLCache = .shared) {
        
        guard let imageURL = URL(string: url) else {
            return
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            let request = URLRequest(url: imageURL)

            if let image = self.cachedImage(for: request) {
                DispatchQueue.main.async {
                    self.image = image
                }
            }
                
            else {
                
                self.fetchImage(from: request) { (image) in
                    DispatchQueue.main.async {
                        self.image = image
                    }
                }
                
            }
        }
    }
    
    private func cachedImage(for request: URLRequest, cache: URLCache = .shared) -> UIImage? {
        
        guard let imageData = cache.cachedResponse(for: request)?.data,
            let cachedImage = UIImage(data: imageData)
            else { return nil }
        
        return cachedImage
    }
    
    private func fetchImage(from request: URLRequest,
                    cache: URLCache = .shared,
                    session: URLSession = .shared,
                    completion: @escaping(UIImage?) -> Void) {
        
        session.dataTask(with: request) { (data, response, error) in
            if error != nil, data == nil {
                completion(nil)
                return
            }
            
            guard let data = data, let image = UIImage(data: data) else {
                completion(nil)
                return
            }
            
            if let response = response {
                let cachedData = CachedURLResponse(response: response, data: data)
                cache.storeCachedResponse(cachedData, for: request)
            }
            
            completion(image)
            
        }.resume()
    }
    
}
