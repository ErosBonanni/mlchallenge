//
//  Double+Currency.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 06/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

extension Double {
    
    var currency: String {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.currencyGroupingSeparator = "."
        numberFormatter.currencyDecimalSeparator = ","
        return numberFormatter.string(for: self) ?? "-"
    }
    
}
