//
//  String+Encoded.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 15/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

extension String {
    
    var urlEncoded: String {
        self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
    }
    
}
