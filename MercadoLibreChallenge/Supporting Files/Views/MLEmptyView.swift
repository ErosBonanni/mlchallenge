//
//  MLEmptyView.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 08/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import UIKit

class MLEmptyView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        backgroundColor = .white
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private let emptyIllustration: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "EmptyIllustration"))
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private let emptyLabel: UILabel = {
        let label = UILabel()
        label.text = "No encontramos ningun producto con esas caracteristicas."
        label.textAlignment = .center
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private func setupConstraints() {
        addSubview(emptyIllustration)
        addSubview(emptyLabel)
        
        NSLayoutConstraint.activate([
            emptyIllustration.centerXAnchor.constraint(equalTo: centerXAnchor),
            emptyIllustration.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            emptyLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
            emptyLabel.topAnchor.constraint(equalTo: emptyIllustration.bottomAnchor, constant: 12),
            emptyLabel.widthAnchor.constraint(equalTo: widthAnchor, constant: -80)
        ])
    }
    
}
