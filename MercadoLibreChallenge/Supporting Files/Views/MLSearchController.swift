//
//  MLSearchController.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 05/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import UIKit

class MLSearchController: UISearchController {
    
    init() {
        super.init(nibName: nil, bundle: nil)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not implemented")
    }
    
    private func setupView() {
        searchBar.isTranslucent = false
        obscuresBackgroundDuringPresentation = false
    }
    
}
