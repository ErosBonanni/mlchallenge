//
//  MLNavigationController.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 05/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import UIKit

class MLNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        navigationBar.prefersLargeTitles = true
    }
    
}
