//
//  ProductViewModel.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

struct ProductViewModel {
    
    private let product: Product
    
    init(_ product: Product) {
        self.product = product
    }
    
    var title: String {
        return product.title
    }
    
    var price: String {
        return product.price.currency
    }
    
    var thumbnail: String {
        return product.thumbnail
    }
    
    var freeShipping: Bool {
        return product.shipping.freeShipping
    }
    
    var condition: String {
        return product.condition?.formatted ?? "Desconocido"
    }
    
    var installments: String? {
        guard let installments = product.installments else {
            return "No hay cuotas disponibles"
        }
        
        return "\(installments.quantity) cuotas de \(installments.amount.currency)"
    }
    
    var sellerPowerStatus: String? {
        return product.seller.powerSellerStatus?.formatted
    }
    
}
