//
//  Product.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

struct Product: Codable {
    
    let id: String
    let siteId: String
    let title: String
    let price: Double
    let currencyId: String
    let thumbnail: String
    let shipping: Shipping
    let condition: Condition?
    let installments: Installments?
    let seller: Seller
    
    enum CodingKeys: String, CodingKey {
        case id
        case siteId = "site_id"
        case title
        case price
        case currencyId = "currency_id"
        case thumbnail
        case shipping
        case condition
        case installments
        case seller
    }
    
}
