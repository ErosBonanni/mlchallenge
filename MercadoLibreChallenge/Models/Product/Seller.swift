//
//  Seller.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 07/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

struct Seller: Codable {
    
    let powerSellerStatus: PowerSellerStatus?
    
    enum CodingKeys: String, CodingKey {
        case powerSellerStatus = "power_seller_status"
    }
    
}

enum PowerSellerStatus: String, Codable {
    case platinum
    case gold
    case silver
    
    var formatted: String {
        switch self {
            case .platinum: return "MercadoLíder Platinum"
            case .gold: return "MercadoLíder Gold"
            case .silver: return "MercadoLíder Silver"
        }
    }
}
