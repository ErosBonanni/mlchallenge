//
//  ProductsResponse.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

struct ProductsResponse: Codable {
    
    let siteId: String
    let results: [Product]
    let paging: ProductsPaging
    
    enum CodingKeys: String, CodingKey {
        case siteId = "site_id"
        case results
        case paging
    }
    
}
