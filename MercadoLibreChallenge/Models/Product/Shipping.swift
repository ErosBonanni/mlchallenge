//
//  Shipping.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 05/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

struct Shipping: Codable {
    let freeShipping: Bool
    let storePickUp: Bool
    
    enum CodingKeys: String, CodingKey {
        case freeShipping = "free_shipping"
        case storePickUp = "store_pick_up"
    }
    
}
