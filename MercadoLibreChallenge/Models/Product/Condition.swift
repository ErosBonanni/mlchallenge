//
//  Condition.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 07/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

enum Condition: String, Codable {
    case new
    case used
    
    var formatted: String {
        switch self {
            case .new: return "Nuevo"
            case .used: return "Usado"
        }
    }
}
