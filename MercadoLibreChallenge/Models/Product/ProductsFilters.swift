//
//  ProductsFilters.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 05/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

protocol ProductFilter {
    var id: String { get }
    var uri: String { get }
}

struct CategoryFilter: ProductFilter {
    
    private let categoryId: String
    
    init(id: String) {
        self.categoryId = id
    }
    
    var id: String { "category" }
    
    var uri: String { "category=\(categoryId)".urlEncoded }
    
}

struct QueryFilter: ProductFilter {
    
    private let query: String
    
    init(_ query: String) {
        self.query = query
    }
    
    var id: String { "query" }
    
    var uri: String { "q=\(query)".urlEncoded }
    
}

protocol FiltersDelegate: class {
    func filtersDidChange()
}

class ProductFilters {
    
    weak var delegate: FiltersDelegate?
    
    private var filters: [ProductFilter] = []
    
    init(_ filters: [ProductFilter] = []) {
        self.filters = filters
    }
    
    func add(_ filter: ProductFilter) {
        removeAll(with: filter.id)
        filters.append(filter)
        delegate?.filtersDidChange()
    }
    
    func remove(_ filter: ProductFilter) {
        removeAll(with: filter.id)
        delegate?.filtersDidChange()
    }
    
    func removeAll() {
        filters.removeAll()
    }
    
    private func removeAll(with id: String) {
        filters.removeAll { $0.id == id }
    }
    
    func uri() -> String {
        filters.compactMap { $0.uri }.joined(separator: "&")
    }
    
}
