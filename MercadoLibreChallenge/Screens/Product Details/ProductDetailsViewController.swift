//
//  ProductDetailsViewController.swift
//  MercadoLibreChallenge
//
//  Created Eros Bonanni on 05/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import UIKit

protocol ProductDetailsViewProtocol: class {
    
    var presenter: ProductDetailsPresenterProtocol? { get set }
    
    func configure(with product: ProductViewModel)
    
}

class ProductDetailsViewController: UIViewController, ProductDetailsViewProtocol {
    
    var presenter: ProductDetailsPresenterProtocol?
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var conditionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var installmentsLabel: UILabel!
    @IBOutlet weak var shippingLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Detalles"
        presenter?.viewDidLoad()
        view.backgroundColor = UIColor(white: 0.97, alpha: 1)
    }

    func configure(with product: ProductViewModel) {
        productImageView.loadImage(from: product.thumbnail)
        conditionLabel.text = product.condition
        titleLabel.text = product.title
        priceLabel.text = product.price
        installmentsLabel.text = product.installments
        shippingLabel.text = product.freeShipping ? "Envío Gratis" : "Envío $450"
    }
    
}
