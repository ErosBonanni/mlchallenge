//
//  ProductDetailsPresenter.swift
//  MercadoLibreChallenge
//
//  Created Eros Bonanni on 05/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import UIKit

protocol ProductDetailsPresenterProtocol: class {
    func viewDidLoad()
}

class ProductDetailsPresenter: ProductDetailsPresenterProtocol {

    weak private var view: ProductDetailsViewProtocol?
    private let router: ProductDetailsRouterProtocol
    private let product: ProductViewModel

    init(interface: ProductDetailsViewProtocol,
         router: ProductDetailsRouterProtocol,
         product: ProductViewModel) {
        self.view = interface
        self.router = router
        self.product = product
    }

    func viewDidLoad() {
        view?.configure(with: product)
    }
    
}
