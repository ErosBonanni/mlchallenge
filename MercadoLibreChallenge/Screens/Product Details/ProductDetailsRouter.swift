//
//  ProductDetailsRouter.swift
//  MercadoLibreChallenge
//
//  Created Eros Bonanni on 05/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import UIKit

protocol ProductDetailsRouterProtocol: class {

}

class ProductDetailsRouter: ProductDetailsRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule(product: Product) -> UIViewController {
        let view = ProductDetailsViewController(nibName: nil, bundle: nil)
        let router = ProductDetailsRouter()
        let productViewModel = ProductViewModel(product)
        let presenter = ProductDetailsPresenter(interface: view,
                                                router: router,
                                                product: productViewModel)
        
        view.presenter = presenter
        router.viewController = view
        
        return view
    }
}
