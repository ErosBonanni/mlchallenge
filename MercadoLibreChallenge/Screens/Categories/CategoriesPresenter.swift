//
//  CategoriesPresenter.swift
//  MercadoLibreChallenge
//
//  Created Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import UIKit

protocol CategoriesSelectorDelegate: class {
    func didSelect(category: Category)
}

protocol CategoriesPresenterProtocol: CategoriesSelectorDelegate {
    func viewDidLoad()
}

class CategoriesPresenter: CategoriesPresenterProtocol {

    weak private var view: CategoriesViewProtocol?
    private let router: CategoriesRouterProtocol
    private let repository: CategoriesRepositoryProtocol
    
    weak var delegate: CategoriesSelectorDelegate?
    
    init(interface: CategoriesViewProtocol,
         router: CategoriesRouterProtocol,
         repository: CategoriesRepositoryProtocol) {
        self.view = interface
        self.router = router
        self.repository = repository
    }
    
    func viewDidLoad() {
        fetchCategories()
    }
    
    func didSelect(category: Category) {
        delegate?.didSelect(category: category)
        router.back()
    }
    
    private func fetchCategories() {
        
        view?.setState(.loading)
        
        repository.categories { (categories, error) in
            
            if let error = error, categories == nil {
                self.view?.setState(.error(description: error.localizedDescription))
                return
            }
            
            guard let categories = categories else {
                self.view?.setState(.error(description: "An unexpected error has occurred"))
                return
            }
            
            DispatchQueue.main.async {
                self.view?.setState(.loaded(categories: categories))
            }
            
        }
    }

}
