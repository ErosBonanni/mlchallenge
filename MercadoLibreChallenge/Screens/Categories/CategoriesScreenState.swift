//
//  CategoriesScreenState.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

enum CategoriesScreenState {
    case loaded(categories: [Category])
    case error(description: String)
    case loading
}
