//
//  CategoriesViewController.swift
//  MercadoLibreChallenge
//
//  Created Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import UIKit

protocol CategoriesViewProtocol: class {
    var presenter: CategoriesPresenterProtocol? { get set }
    func setState(_ state: CategoriesScreenState)
}

class CategoriesViewController: UITableViewController, CategoriesViewProtocol {
    
    var presenter: CategoriesPresenterProtocol?
    private let cellId = "categoryCellId"
    private var categories: [Category] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        setupView()
    }
    
    private func setupView() {
        title = "Categorías"
        setActivityIndicatorLayout()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
    }
    
    private func setActivityIndicatorLayout() {
        view.addSubview(activityIndicator)
        activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    func setState(_ state: CategoriesScreenState) {
        switch state {
            case .loaded(let categories): setLoadedState(categories: categories)
            case .error(let errorDescription): setErrorState(message: errorDescription)
            case .loading: setLoadingState()
        }
    }
    
    private func setLoadedState(categories: [Category]) {
        activityIndicator.alpha = 0
        self.categories = categories
        tableView.alpha = 1
    }
    
    private func setErrorState(message: String) {
        tableView.alpha = 0
        activityIndicator.alpha = 0
        // TODO: Show error message.
        print(message)
    }
    
    private func setLoadingState() {
        tableView.alpha = 0
        activityIndicator.alpha = 1
    }
    
    private let activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.startAnimating()
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        return activityIndicator
    }()
    
}

extension CategoriesViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        
        let category = categories[indexPath.row]
        cell.textLabel?.text = category.name
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let selectedCategory = categories[indexPath.row]
        presenter?.didSelect(category: selectedCategory)
    }
    
}
