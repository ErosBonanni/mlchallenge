//
//  CategoriesRouter.swift
//  MercadoLibreChallenge
//
//  Created Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import UIKit

protocol CategoriesRouterProtocol: class {
    func back()
}

class CategoriesRouter: CategoriesRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule(delegate: CategoriesSelectorDelegate) -> UIViewController {
        let view = CategoriesViewController()
        let router = CategoriesRouter()
        let presenter = CategoriesPresenter(interface: view,
                                            router: router,
                                            repository: CategoriesRepository())
        
        presenter.delegate = delegate
        view.presenter = presenter
        router.viewController = view
        
        return view
    }
    
    func back() {
        viewController?.navigationController?.popViewController(animated: true)
    }
    
}
