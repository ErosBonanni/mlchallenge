//
//  ProductsPresenter.swift
//  MercadoLibreChallenge
//
//  Created Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import UIKit

protocol ProductsPresenterProtocol: class {
    func viewDidLoad()
    func didSearch(for productName: String)
    func didSelect(product: Product)
    func didTapSelectCategory()
    func removeFilters()
}

class ProductsPresenter: ProductsPresenterProtocol, CategoriesSelectorDelegate, FiltersDelegate {

    weak private var view: ProductsViewProtocol?
    private let router: ProductsRouterProtocol
    private let repository: ProductsRepositoryProtocol
    private let filters: ProductFilters
    
    init(interface: ProductsViewProtocol,
         router: ProductsRouterProtocol,
         repository: ProductsRepositoryProtocol,
         filters: ProductFilters) {
        self.view = interface
        self.router = router
        self.repository = repository
        self.filters = filters
        
        self.filters.delegate = self
    }
    
    func viewDidLoad() {
        fetchProducts()
    }
    
    func didSelect(product: Product) {
        router.showProductDetails(product)
    }
    
    func didSearch(for productName: String) {
        filters.add(QueryFilter(productName))
    }
    
    func didTapSelectCategory() {
        router.showCategorySelector(with: self)
    }
    
    func didSelect(category: Category) {
        filters.add(CategoryFilter(id: category.id))
        view?.setCategory(title: category.name)
    }
    
    func removeFilters() {
        filters.removeAll()
    }
    
    func filtersDidChange() {
        fetchProducts()
    }
    
    private func fetchProducts() {
        
        view?.set(state: .loading)
        
        repository.products(for: filters) { (productsResponse, error) in
            DispatchQueue.main.async {
                if let error = error, productsResponse == nil {
                    self.view?.set(state: .error(description: error.localizedDescription))
                    return
                }
                
                guard let products = productsResponse?.results else {
                    self.view?.set(state: .error(description: "An unexpected error has occurred"))
                    return
                }
                
                if products.count == 0 {
                    self.view?.set(state: .empty)
                    return
                }
                
                self.view?.set(state: .loaded(products: products))
            }
            
        }
    }
    
}
