//
//  ProductsScreenState.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import Foundation

enum ProductsScreenState {
    case empty
    case loaded(products: [Product])
    case error(description: String)
    case loading
}
