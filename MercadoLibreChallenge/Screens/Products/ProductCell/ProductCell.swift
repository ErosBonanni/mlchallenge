//
//  HomeProductCell.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var freeShippingLabel: UILabel!
    
    func configure(with product: ProductViewModel) {
        titleLabel.text = product.title
        priceLabel.text = product.price
        productImageView.loadImage(from: product.thumbnail)
        freeShippingLabel.isHidden = !product.freeShipping
    }
    
}
