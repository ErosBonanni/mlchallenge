//
//  CategoryHeader.swift
//  MercadoLibreChallenge
//
//  Created by Eros Bonanni on 07/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import UIKit

protocol CategoryHeaderDelegate: class {
    func didTapSelectCategory()
    func didTapRemoveFilter()
}

class CategoryHeader: UIView {
    
    weak var delegate: CategoryHeaderDelegate?
    
    private let selectTitle = "Seleccionar categoría"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor(white: 0.97, alpha: 1)
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("Not implemented")
    }
    
    func setCategory(title: String) {
        button.setTitle(title, for: .normal)
        removeFilterButton.isHidden = title == selectTitle
    }
    
    private lazy var button: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(selectTitle, for: .normal)
        button.addTarget(self, action: #selector(handleTap), for: .touchUpInside)
        return button
    }()
    
    private lazy var removeFilterButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(systemName: "xmark.circle.fill"), for: .normal)
        button.tintColor = .lightGray
        button.isHidden = true
        button.addTarget(self, action: #selector(handleRemoveFilter), for: .touchUpInside)
        return button
    }()
    
    @objc private func handleTap() {
        delegate?.didTapSelectCategory()
    }
    
    @objc private func handleRemoveFilter() {
        setCategory(title: selectTitle)
        delegate?.didTapRemoveFilter()
    }
    
    private func setupConstraints() {
        
        addSubview(button)
        addSubview(removeFilterButton)
        
        NSLayoutConstraint.activate([
            button.centerYAnchor.constraint(equalTo: centerYAnchor),
            button.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
            
            removeFilterButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            removeFilterButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20)
        ])
    }
    
}
