//
//  ProductsRouter.swift
//  MercadoLibreChallenge
//
//  Created Eros Bonanni on 04/12/2019.
//  Copyright © 2019 Eros Bonanni. All rights reserved.
//

import UIKit

protocol ProductsRouterProtocol: class {
    func showProductDetails(_ product: Product)
    func showCategorySelector(with delegate: CategoriesSelectorDelegate)
}

class ProductsRouter: ProductsRouterProtocol {
    
    weak var viewController: UIViewController?
    
    static func createModule(for filters: [ProductFilter]) -> UIViewController {
        let view = ProductsViewController()
        let router = ProductsRouter()
        let productsRepository = ProductsRepository()
        let presenter = ProductsPresenter(interface: view,
                                          router: router,
                                          repository: productsRepository,
                                          filters: ProductFilters([QueryFilter("iPhone")]))
        
        view.presenter = presenter
        router.viewController = view
        
        return view
    }
    
    func showProductDetails(_ product: Product) {
        let details = ProductDetailsRouter.createModule(product: product)
        viewController?.navigationController?.pushViewController(details, animated: true)
    }
    
    func showCategorySelector(with delegate: CategoriesSelectorDelegate) {
        let categories = CategoriesRouter.createModule(delegate: delegate)
        viewController?.navigationController?.pushViewController(categories, animated: true)
    }
    
}
